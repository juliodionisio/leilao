package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances = new ArrayList<Lance>();

    public Leilao() {

    }

    public List<Lance> getLances() {
        return lances;
    }

    public void adicionarLance(Lance lance) {
        if(lances.size() > 0){
            if(lance.getValor() > retornaUltimoLance().getValor()) {
                lances.add(lance);
            }
        }else lances.add(lance);

    }

    public Lance retornaUltimoLance(){
        return lances.get(lances.size() - 1);
    }
}
