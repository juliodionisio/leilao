package br.com.leilao;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public double retornaMaiorLance(){
        return leilao.retornaUltimoLance().getValor();
    }

    public String retornaDonoDoMaiorLance(){
        return leilao.retornaUltimoLance().getUsuario().getNome();
    }
}
