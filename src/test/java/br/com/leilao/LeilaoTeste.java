package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {


    private Leilao leilao;
    private Usuario usuario;
    private Lance lance;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        lance = new Lance();
        leilao  = new Leilao();

        usuario.setNome("Kronemberg");
        usuario.setId(1);
        lance.setValor(5000);
        lance.setUsuario(usuario);
        leilao.adicionarLance(lance);

    }

    @Test
    public void testaAdicionarNovoLance(){

        Assertions.assertEquals(lance, leilao.retornaUltimoLance());
    }

    @Test
    public void testaAdicionarLanceMenorQueAnterior(){
        usuario = new Usuario();
        usuario.setNome("Pirpimpibles");
        usuario.setId(2);
        lance = new Lance();
        lance.setValor(4000);
        lance.setUsuario(usuario);
        leilao.adicionarLance(lance);
        Assertions.assertNotSame(lance, leilao.retornaUltimoLance());
    }

    @Test
    public void testaAdicionarLanceMaiorQueAnterior(){
        usuario = new Usuario();
        usuario.setNome("Morty");
        usuario.setId(3);
        lance = new Lance();
        lance.setValor(6000);
        lance.setUsuario(usuario);
        leilao.adicionarLance(lance);
        Assertions.assertEquals(lance, leilao.retornaUltimoLance());
    }
}
