package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

public class LeiloeiroTeste {

    private Leilao leilao;
    private Usuario usuario;
    private Lance lance;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        lance = new Lance();
        leilao  = new Leilao();
        leiloeiro = new Leiloeiro();
        leiloeiro.setLeilao(leilao);
        leiloeiro.setNome("Rick Sanchez");

        usuario.setNome("Kronemberg");
        usuario.setId(1);
        lance.setValor(5000);
        lance.setUsuario(usuario);
        leilao.adicionarLance(lance);

        usuario = new Usuario();
        lance = new Lance();

        usuario.setNome("Morty");
        usuario.setId(1);
        lance.setValor(6000);
        lance.setUsuario(usuario);
        leilao.adicionarLance(lance);


    }

    @Test
    public void testaRetornaMaiorLance(){

        Assertions.assertEquals(6000, leiloeiro.retornaMaiorLance());
    }

    @Test
    public void testaRetornaDonoDoMaiorLance(){

        Assertions.assertEquals(usuario.getNome(), leiloeiro.retornaDonoDoMaiorLance());
    }
}
